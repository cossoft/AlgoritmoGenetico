from Poblacion import Poblacion
from Individuo import Individuo
import random
class AlgoritmoGenetico():
	def __init__(self):
		self.mutationRate = 0.1
		self.iteraciones = 0
		self.poblacion = Poblacion()
		self.elitismo = True
		
	def primeraGeneracion(self,puntoA,puntoB,grafo):
		self.poblacion.inicializar(puntoA,
								   puntoB,
								   grafo)
		self.adaptacion(grafo)
		
	def obtenerGeneracion(self):
		return self.poblacion
		
	def restartMap(self):
		self.poblacion.deleteGeneracion()
		self.iteraciones = 0
		
	def adaptacion(self, grafo):
		best = self.poblacion.obtenerFittest()
		print('Funcion: ' + str(best.obtenerFitness()))
		individuos = self.poblacion.obtenerIndividuos()
		padre = self.seleccionMejorAdaptado(individuos, None)
		madre = self.seleccionMejorAdaptado(individuos, padre)
		caminoHijoA = self.cruzar(padre, madre, grafo)
		caminoHijoB = self.cruzar(padre, madre, grafo)
		hijoA = [caminoHijoA, self.nuevosGenes(grafo, caminoHijoA)]
		hijoB = [caminoHijoA, self.nuevosGenes(grafo, caminoHijoA)]
		self.nuevaGeneracion(hijoA, hijoB, grafo)
		
	def nuevaGeneracion(self,hijoA, hijoB, grafo):
		self.poblacion.deleteGeneracion()
		for i in range (0, 40):
			if random.random() <=  self.mutationRate:
				if i <= 2:
					self.poblacion.addIndividuo(self.mutar(hijoA, grafo))
				else:
					self.poblacion.addIndividuo(self.mutar(hijoB, grafo))
			elif random.randrange(1,3) == 1:
				individuo = Individuo()
				individuo.setGenes(hijoA[1])
				individuo.setCamino(hijoA[0])
				self.poblacion.addIndividuo(individuo)
			else: 
				individuo = Individuo()
				individuo.setGenes(hijoB[1])
				individuo.setCamino(hijoB[0])
				self.poblacion.addIndividuo(individuo)
		best = self.poblacion.obtenerFittest()
		#print('Nueva Ruta: ' + str(best.obtenerCamino()))
		#print('Mejor Ajuste: ' + str(best.obtenerGenes()))
		self.iteraciones += 1
		if self.iteraciones < 40:
			self.adaptacion(grafo)
			
		
	def cruzar(self, padre, madre, grafo):
		caminoPadre = padre.obtenerCamino()
		caminoMadre = madre.obtenerCamino()
		cruze = []
		for i in range (0,len(caminoPadre)):
			for trazo in caminoMadre:
				if (i != 0 and
					i != len(caminoPadre) - 1 and
					trazo == caminoPadre[i] and
					trazo not in cruze):
					cruze.append(trazo)
		if cruze:
			if len(cruze) == 1:
				indexCruze = caminoMadre.index(cruze[0]) + 1
				return self.cruzarGenes(caminoPadre,
										caminoMadre,
										indexCruze, grafo)
			else:
				index = random.randrange(0,len(cruze))
				indexCruze = caminoMadre.index(cruze[index]) + 1
				return self.cruzarGenes(caminoPadre,
										caminoMadre,
										indexCruze, grafo)
		return caminoPadre
				
	def cruzarGenes(self, padre, madre, indexGen, grafo):
		if random.random() >= 0.5:
			caminoHijo = self.infront(padre ,madre, indexGen)
			genHijo = self.sumarGenes(self.nuevosGenes(grafo, caminoHijo))
			genPadre = self.sumarGenes(self.nuevosGenes(grafo,padre))
			if genHijo < genPadre:
				return caminoHijo 
		else:
			caminoHijo = self.behind(padre, madre, indexGen)
			genHijo = self.sumarGenes(self.nuevosGenes(grafo, caminoHijo))
			genPadre = self.sumarGenes(self.nuevosGenes(grafo,padre))
			if genHijo < genPadre:
				return caminoHijo
		return padre
		
			
	def infront(self,front, back,index):
		crossPos = front.index(back[index-1]) + 1
		return back[0:index] + front[crossPos:len(front)]
		
	def behind(self, front, back,index):
		crossPos = front.index(back[index-1]) + 1
		return front[0:crossPos] + back[index:len(back)]
	
	def mutar(self, individuo, grafo):
		#print('Empieza Mutacion...' + str(individuo[0]))
		original = Individuo()
		original.setCamino(individuo[0])
		original.setGenes(individuo[1])
		if random.random() >= 0.5:
			mutante = self.mutarOnFront(original, grafo)
			if mutante.obtenerFitness() <= original.obtenerFitness():
				return mutante
			return original
		else:
			mutante = self.mutarOnBack(original, grafo)
			if mutante.obtenerFitness() <= original.obtenerFitness():
				return mutante
			return original
		   
	def mutarOnFront(self, individuo, grafo):
		#print('Front Mutation...')
		indexMutar = 0
		mutante = Individuo()
		caminoIndividuo = individuo.obtenerCamino()
		if len(caminoIndividuo) == 2:
			return individuo
		elif len(caminoIndividuo) == 3:
			indexMutar = 0
		elif len(caminoIndividuo) > 3:
			indexMutar = random.randrange(0,len(caminoIndividuo) - 2)
		
		genInmutable = []
		if indexMutar != 0:
			genInmutable = caminoIndividuo[0:indexMutar]
			
		inicioMutacion = caminoIndividuo[indexMutar]
		#print('StepMutacion: ' + str(inicioMutacion))
		# Genrar Nuevo tramo para mutar individuo
		# Verificar que el nuevo tramo de individuo este correcto
		correcto = False
		while correcto is False:
			mutante.restart() #Si no es correcto reiniciar busqueda
			mutante.setCamino(individuo.obtenerCamino()[0:indexMutar])
			mutante.puntoInicio(inicioMutacion)
			mutante.puntoDestino(individuo.obtenerDestino())
			correcto = mutante.generarIndividuo(inicioMutacion, grafo)
		
		nuevoCamino = mutante.obtenerCamino() # Obtener nuevo camino generado
		nuevosGenes = self.nuevosGenes(grafo,nuevoCamino)
		#print('Path Front Mutado: ' + str(nuevoCamino))
		mutante.setGenes(nuevosGenes)         # Guarda genes de nuevo camino
		return mutante
		
	def mutarOnBack(self, individuo, grafo):
		#print('Back Mutation...')
		caminoIndividuo = individuo.obtenerCamino()
		indexMutar = 0
		if len(caminoIndividuo) == 2:
			return individuo
		elif len(caminoIndividuo) == 3:
			indexMutar = 2
		else:
			indexMutar = random.randrange(2,len(caminoIndividuo))
		genInmutable = []
		if indexMutar + 1 != len(caminoIndividuo):
			genInmutable = caminoIndividuo[indexMutar+1:len(caminoIndividuo)]
			#print('Gen Inmutable: ' + str(genInmutable))
		inicioMutacion = caminoIndividuo[0]
		finMutacion = caminoIndividuo[indexMutar]
		#print('StepMutacion: ' + str(finMutacion))
		mutante = Individuo()
		correcto = False
		while correcto is False:
			mutante.restart() #Si no es correcto reiniciar busqueda
			mutante.puntoInicio(inicioMutacion)
			mutante.puntoDestino(finMutacion)
			correcto = mutante.generarIndividuo(inicioMutacion, grafo)
			for step in genInmutable:
				if step in mutante.obtenerCamino():
					correcto = False
		nuevoCamino = mutante.obtenerCamino() + genInmutable
		nuevosGenes = self.nuevosGenes(grafo,nuevoCamino)
		mutante.setGenes(nuevosGenes)
		mutante.setCamino(nuevoCamino)
		#print('Path Back Mutado: ' + str(nuevoCamino))
		return mutante
		
				
	def seleccionMejorAdaptado(self, individuos, otro):
		individuoAdaptado = individuos[0]
		for indv in individuos:
			if (indv.obtenerFitness() <= individuoAdaptado.obtenerFitness() and
				otro != indv):
				individuoAdaptado = indv
		return individuoAdaptado
		
	def nuevosGenes(self, grafo, hijo):
		genHijo = []
		for i in range (0,len(hijo)-1):
			for link in grafo.aristas:             #Recorrer las conexiones del mapa
				if ((link.getInicial() == hijo[i] and
					 link.getTerminal() == hijo[i+1]) or
					(link.getTerminal() == hijo[i] and
					 link.getInicial() == hijo[i+1])):     #Punto partida c.otro nodo
					genHijo.append(link.getPeso())
		return genHijo
	
	def sumarGenes(self, genes):
		suma = 0
		for gen in genes:
			suma += float(gen)
		return suma
