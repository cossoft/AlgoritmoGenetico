import sys
import copy
import random
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from Punto import Punto
from Grafo import Grafo
from Arista import Arista
from Individuo import Individuo
from Poblacion import Poblacion
from AlgoritmoGenetico import AlgoritmoGenetico
from Places import *
class LienzoDibujo(QtGui.QWidget):
	'''
	Clase que implementa el lienzo en donde se grafica los puntos y aristas
	del grafo, es un objeto de tipo Widget, el cual contiene metodos para 
	los eventos de acciones de usuario con el mouse, este objeto tiene referencia
	del la ventana principal, lo que permite la interaccion con la interfaz externa
	'''
	def __init__(self, ventana):
		'''
		Metodo para iniciar el widget, aqui se instancian, puntos de referencia 
		de las acciones del mouse, se inicia un graficador de tipo QPainter y algunas otras
		variables necesarioas
		:ventana: es la referencia de venta principal es de tipo QWindow
		'''
		QWidget.__init__(self)

		self.lienzo = ventana
		self.painter = QPainter()
		self.puntoA = None
		self.PuntoB = None
		self.empezar = False
		self.trazoIndividuo = []
		self.AG = AlgoritmoGenetico()	
 
	def paintEvent(self, event):
		'''
		Metodo que pinta los objetos creados por el usuario, tambien este
		detecta los eventos del raton en caso de pintar arista
		Aqui se dibuja Puntos con sus respectivos nombre, Aristas con
		los respectivos pesos, y en caso de aplicar Kruskal tambien 
		dibuja el arbol de axpansion minima 
		'''		
		self.painter.begin(self)
		self.painter.setRenderHint(QPainter.Antialiasing)
			
		for arista in self.lienzo.aristas:
			inicial = arista.puntoA.ubicacion
			terminal = arista.puntoB.ubicacion
			self.painter.setPen(QPen(Qt.darkCyan, 2, Qt.DotLine))
			self.painter.drawLine(inicial.x(),
			                      inicial.y(),
			                      terminal.x(),
			                      terminal.y())
		
		if self.empezar:
			self.painter.setPen(QPen(QBrush(Qt.darkGreen), 5, Qt.DashLine))
			self.painter.drawEllipse(self.trazoIndividuo[0][0].getUbicacion(),30,30)
			self.painter.setPen(QPen(QBrush(Qt.red), 5, Qt.DashLine))
			self.painter.drawEllipse(self.trazoIndividuo[-1][-1].getUbicacion(),30,30)
			colors = [(Qt.black),(Qt.darkMagenta), (Qt.darkRed),
			          (Qt.gray), (Qt.green),
			          (Qt.blue), (Qt.magenta),
			          (Qt.yellow), (Qt.cyan)]
			size = 12
			for individuo in self.trazoIndividuo:
				self.painter.setPen(QPen(QBrush(colors[size-4]),
				                    size-2,
				                    Qt.DashLine))
				size -= 2
				for i in range (0,len(individuo) - 1):
					inicial = individuo[i].getUbicacion()
					terminal = individuo[i+1].getUbicacion()
					self.painter.drawLine(inicial.x(),
					                      inicial.y(),
					                      terminal.x(),
					                      terminal.y())
						
			                       
		for puntos in self.lienzo.puntos:
			point = puntos.getUbicacion()
			name = puntos.getNombre()
			image = QImage('Places/' + puntos.imagen)
			
			self.painter.setPen(QPen(Qt.black))
			font = QFont('Times, Normal', 8, 0, True)				
			self.painter.setFont(font)
			self.painter.drawImage(point.x() -20, point.y() -20, image)
			#self.painter.drawText(point.x() - 20,
			#					  point.y() - 18,
			#					  name)
			
			
		for arista in self.lienzo.aristas:
			inicial = arista.puntoA.ubicacion
			terminal = arista.puntoB.ubicacion
			font = QFont('Times, Bold', 11, 0, True)				
			self.painter.setFont(font)
			self.painter.setPen(QPen(Qt.black))
			self.painter.drawText((inicial.x() + terminal.x()) / (2),
			                      (inicial.y() + terminal.y())/ (2),
			                       str(arista.peso))
			                           
		self.painter.end()
		
	def mousePressEvent(self, event):
		'''
		Metodo que captura la acciones de los botones del mouse
		cuando se quiere crear un punto aqui se guarda el punto en 
		donde si ha clikeado, pero este punto debe estar fuera de algo de los
		puntos si es que ya existe mas puntos, si se clumplen las condiciones
		se habilita las opciones para cargar el nombre del puerto
		que sera el nombre del punto
		Para crer arista, se camptura las acciones del boton izquierdo del mouse
		y si se ha clikeado en alguno de los puntos, se guarda el punto 
		como punto inicial
		'''
		if (event.buttons() == QtCore.Qt.LeftButton):  #Si el evento de raton es clickDerecho
			for n in self.lienzo.puntos:
				if n.ecuacionDeCirculo(event.pos()):
					if self.puntoA is None:
						self.puntoA = n.getNombre()
						self.lienzo.labelMessage.setText('Seleccione Punto B')
					elif self.puntoA != n.getNombre():
						self.puntoB = n.getNombre()
						self.startAG()
					else:
						self.lienzo.labelMessage.setText('Seleccione Punto Diferente !')
					return

	def startAG(self):
		self.trazoIndividuo = []
		self.empezar = False
		self.AG.restartMap()
		self.AG.primeraGeneracion(self.puntoA,
		                          self.puntoB,
		                          self.lienzo.grafo)
		best = self.AG.obtenerGeneracion().obtenerFittest()
		self.lienzo.labelMessage.setText('Individuo Mejor Adaptado Tiene: ' +
		                                 str(best.obtenerFitness()) +
		                                 'u de extension')
		self.crearGrafo(best)
		self.puntoA = None
		self.puntoB = None
				
	def cambiarGrafo(self, nuevo):
		'''
		Metodd secundario que se utiliza para la funcion de aplicarKruskal
		esta funcion agrega las aristas que tienen el camino mas corto
		y seran las que posteriormente se graficaran, ya que la varible estado para
		a ser True en este metodo
		:nuevo: es el grafo creado con el algoritmo
		ya que este grafo solo contendra las aristas indicadas
		'''
		for n in self.lienzo.aristas:
			aux = n.getArista()
			if nuevo.buscarArista(aux) == True:
				n.setColor(QPen(Qt.green))
				self.neo.append(n)
		self.estado = True
		self.repaint()
		
	def crearGrafo(self, individuo):
		trazoIndividuo = []
		for nodo in individuo.camino:
			for punto in self.lienzo.puntos:
				if nodo == punto.getNombre():
					trazoIndividuo.append(punto)
		self.trazoIndividuo.append(trazoIndividuo)
		trazoIndividuo = []
		self.empezar = True
		self.AG.iteraciones = 0
		self.repaint()
