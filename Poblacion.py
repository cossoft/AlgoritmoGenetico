from Individuo import Individuo
class Poblacion():
	def __init__(self):
		self.individuos =[]
		self.sizePoblacion = 20
		
	def inicializar(self, puntoA, puntoB, grafo):
		for i in range(0, self.sizePoblacion):
			correcto = False
			individuo = Individuo()
			while correcto is False:
				individuo.restart()								
				individuo.puntoInicio(puntoA)
				individuo.puntoDestino(puntoB)
				correcto = individuo.generarIndividuo(puntoA,
													  grafo)
			self.individuos.append(individuo)	
					
	def obtenerIndividuo(self, index):
		return self.individuos[index]
		
	def obtenerFittest(self):
		fittest = self.individuos[0]
		for individuo in self.individuos:
			if fittest.obtenerFitness() >= individuo.obtenerFitness():
				fittest = individuo
		return fittest
		
	def deleteGeneracion(self):
		self.individuos = []
		
	def addIndividuo(self,individuo):
		self.individuos.append(individuo)
	
	def obtenerIndividuos(self):
		return self.individuos
