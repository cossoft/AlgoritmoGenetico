lista =[['alaska.png',0,0,'Alaska'],
        ['alemania.png',0,0,'Branderburgo'],
        ['antartida.png',0,0,'Antartida'],
        ['birmania.png',0,0,'Shwedagon'],
        ['bogota.png',0,0,'Bogota'],
        ['bolivia.png',0,0,'Tihuanaco'],
        ['camboya.png',0,0,'Siem Reap'],
        ['canada.png',0,0,'Torre CN'],
        ['china.png',0,0,'Muralla China'],
        ['costarica.png',0,0,'Monteverde'],
        ['egipto.png',0,0,'Egipto'],
        ['filipinas.png',0,0,'Colinas Chocolate'],
        ['galapagos.png',0,0,'Galapagos'],
        ['grecia.png',0,0,'Santorini'],
        ['guatemala.png',0,0,'Tikal'],
        ['hungria.png',0,0,'Budapest'],
        ['iandres',0,0,'San Andres'],
        ['iguazu.png',0,0,'Iguazu'],
        ['india.png',0,0,'Taj Mahal'],
        ['japon.png',0,0,'Japón'],
        ['jordania.png',0,0,'Petra'],
        ['lahabana.png',0,0,'La Habana'],
        ['lisboa.png',0,0,'Lisboa'],
        ['londres.png',0,0,'Big Ben'],
        ['madagascar.png',0,0,'Madagascar'],
        ['madrid.png',0,0,'Madrid'],
        ['mexico.png',0,0,'Palacio Bellas Artes'],
        ['mexico2.png',0,0,'Chichen Itza'],
        ['milan.png',0,0,'Catedral Milan'],
        ['opera.png',0,0,'Opera de Sidney'],
        ['paris.png',0,0,'Torre Eiffel'],
        ['peru.png',0,0,'Machu Picchu'],
        ['pisa.png',0,0,'Torre de Pisa'],
        ['quito.png',0,0,'Quito'],
        ['rio.png',0,0,'Rio de Janeiro'],
        ['roma.png',0,0,'Coliseo Romano'],
        ['sanfran.png',0,0,'San Francisco'],
        ['sudafrica.png',0,0,'Sudafrica'],
        ['suiza.png',0,0,'Cervino'],
        ['tailandia.png',0,0,'Bangkok'],
        ['turquia.png',0,0,'Aya Sofia'],
        ['uruguay.png',0,0,'Uruguay'],
        ['usa.png',0,0,'New York'],
        ['venezuela.png',0,0,'Playa del Agua'],
        ['vietnam.png',0,0,'Vietnam']]

def obtenerNombreImagen(nombrePais):
	imagen = ''
	for elements in lista:
		if nombrePais == elements[3]:
			imagen = elements[0]
	return imagen
			
	    
def obtenerCiudades(nomCiudades=[]):
	for ciudades in lista:
		nomCiudades.append(ciudades[3])
	return nomCiudades
